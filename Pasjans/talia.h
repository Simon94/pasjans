#pragma once
#include "karta.h"

class Talia
{

public:
	Karta * Head;
	Karta * Tail;
	Karta * Current;
	Karta * tmp;
	int wydane_karty[5][8];
	int pozosta�e_karty[12];

	void SzukajKarty(Karta* &Head, int id);
	void SzukajKartyDodatkowej(Karta* &Head, int id);
	Talia();

	~Talia();
};

